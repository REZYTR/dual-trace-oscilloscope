# Dual Trace Oscilloscope

A dual trace oscilloscope with a blue pill (STM32F103C8) and a 1.8 inch TFT screen (160x128)
In my case (see code) it is overclocked on 120 MHz with the ADC on 30 MHz.

Timebase from 20us to 200ms / division
Sensitivity from 500mV to 2V / division
