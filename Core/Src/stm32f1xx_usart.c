#include "stm32f1xx.h"
#include "stm32f1xx_ll_usart.h"
#include <stm32f1xx_usart.h>


#if (USART_RXBUFF_SIZE & USART_RXBUFF_MASK)
#error RX BUFFER SIZE is not a power of 2
#endif
#if (USART_TXBUFF_SIZE & USART_TX_BUFF_MASK)
#error USART TXBUFF SIZE is not a power of 2
#endif


volatile uint8_t	USART_rxbuff[USART_RXBUFF_SIZE];
volatile uint8_t	USART_txbuff[USART_RXBUFF_SIZE];
volatile uint8_t	USART_rxhead = 0;
volatile uint8_t	USART_rxtail = 0;
volatile uint8_t	USART_txhead = 0;
volatile uint8_t	USART_txtail = 0;
volatile uint8_t 	USART_RX_ERROR = 0;

void USART_init()
{
	LL_USART_EnableIT_RXNE(USART1);
}

uint8_t USART_getc(void)
{
	uint8_t tmp;
	uint8_t data;

	if (USART_rxhead == USART_rxtail)
	{
		return(0); //no data available
	}

	tmp = (USART_rxtail + 1) & USART_RXBUFF_MASK;
	data = USART_rxbuff[tmp];
	USART_rxtail = tmp;
	return(data);
}


void USART_putc(uint8_t data)
{
	uint8_t tmp;

	tmp = (USART_txhead + 1) & USART_TXBUFF_MASK;
	while (tmp == USART_txtail) {}                            //wait for free space in tx buffer

	USART_txbuff[tmp] = data;
	USART_txhead = tmp;

	LL_USART_EnableIT_TXE(USART1);
}


void USART_puts(char *s)
{
	while (*s) USART_putc(*s++);
}


uint8_t USART_available(void)
{
	return((USART_RXBUFF_MASK + USART_rxhead - USART_rxtail) % USART_RXBUFF_MASK);
}


void USART_flush(void)
{
	USART_rxhead = USART_rxtail;
}


void USART1_IRQHandler(void)
{
	uint8_t tmp;
	uint8_t data;

	if (LL_USART_IsActiveFlag_RXNE(USART1))						//data is ontvangen
	{
		USART_RX_ERROR = 0;
		data = (uint8_t) LL_USART_ReceiveData8(USART1) & 0xFF;
		tmp = (USART_rxhead + 1) & USART_RXBUFF_MASK;
		if (tmp == USART_rxtail)
		{
			USART_RX_ERROR = 1;
		}
		else
		{
			USART_rxbuff[tmp] = data;
			USART_rxhead = tmp;
		}
		LL_USART_ClearFlag_RXNE(USART1);         				//data is verwerkt, clear flag
	}



	if (LL_USART_IsActiveFlag_TXE(USART1))             			//data moet worden verzonden
	{
		if (USART_txhead == USART_txtail)
		{
			LL_USART_DisableIT_TXE(USART1);  					//tx buffer empty, disable TXE interrupt
		}
		else
		{
			tmp = (USART_txtail + 1) & USART_TXBUFF_MASK;
			USART_txtail = tmp;
			LL_USART_TransmitData8(USART1, USART_txbuff[tmp]);
		}
		LL_USART_ClearFlag_TC(USART1);          				//data wordt verzonden, clear flag
	}
}