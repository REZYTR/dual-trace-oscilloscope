#define TIMEBASE_MODE 			0
#define TRIGGER_LEVEL_MODE 		1
#define TRIGGER_CHANNEL_MODE	2

#define ATTENUATOR1_MODE 		0
#define ATTENUATOR2_MODE 		1
#define VIRTUAL_GROUND_MODE 	2


#include "main.h"
#include "stm32f1xx_it.h"

volatile int16_t timeout;

extern volatile int8_t conversion_ready;											//0, 1

extern volatile int8_t function_button_encoder1;									//0, 1			timebase, triggerlevel
extern volatile int8_t function_button_encoder2;									//0, 1, 2, 3  	atten1, atten2, virtual_ground1

extern volatile int8_t timebase;													//0 .. 13		32us .. 200ms / division
extern volatile int16_t trigger_level;												//0 .. 127		top to bottom of screen
extern volatile int8_t trigger_channel;												//0, 1			channel1, channel2

extern volatile int8_t attenuator_ch1;												//0, 1, 2, 3	500mV, 1V, 2V, 5V / division
extern volatile int8_t attenuator_ch2;												//0, 1, 2, 3	500mV, 1V, 2V, 5V / division
extern volatile int16_t virtual_ground;												//0 .. 127		top to bottom of screen

extern volatile uint16_t attenuator_ch1_color;
extern volatile uint16_t attenuator_ch2_color;
extern volatile uint16_t timebase_color;
extern volatile uint16_t trigger_level_color;
extern volatile uint16_t trigger_channel_color;
extern volatile uint16_t virtual_ground_color;

volatile int8_t rotary_1_updown;
volatile int8_t rotary_2_updown;



void NMI_Handler(void)
{

}


void HardFault_Handler(void)
{
	while (1)
	{

	}
}


void MemManage_Handler(void)
{
	while (1)
	{

	}
}


void BusFault_Handler(void)
{
	while (1)
	{

	}
}


void UsageFault_Handler(void)
{
	while (1)
	{

	}
}


void SVC_Handler(void)
{

}


void DebugMon_Handler(void)
{

}


void PendSV_Handler(void)
{

}


void SysTick_Handler(void)
{
	if (timeout > 0) timeout--;													//when enabled: every 1 ms
	else
	{
		LL_TIM_DisableIT_UPDATE(TIM2);											//disable debouncing timer interrupt

		LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_6);									//enable rotary encoder interrupts
		LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_8);
		LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_11);
		LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_12);

		LL_SYSTICK_DisableIT();													//disable THIS interrupt
	}
}


void DMA1_Channel1_IRQHandler(void)
{																				//as soon as DMA is ready stop TIM3
	LL_TIM_DisableCounter(TIM3);												//so no more triggering of the ADCs
	if (LL_DMA_IsActiveFlag_TC1(DMA1))
	{
		while(LL_DMA_IsActiveFlag_TC1(DMA1)) LL_DMA_ClearFlag_TC1(DMA1);
		conversion_ready = 1;
	}
}


void TIM2_IRQHandler(void)														//10 kHz when enabled
{
	static uint8_t rotary_2_pinA;
	static uint8_t rotary_2_pinB;
	static uint8_t rotary_1_pinA;
	static uint8_t rotary_1_pinB;

	if (LL_TIM_IsActiveFlag_UPDATE(TIM2))
	{
		while (LL_TIM_IsActiveFlag_UPDATE(TIM2)) LL_TIM_ClearFlag_UPDATE(TIM2);

		rotary_1_updown = 0;
		rotary_2_updown = 0;

		rotary_1_pinA <<= 1;
		rotary_1_pinB <<= 1;
		rotary_2_pinA <<= 1;
		rotary_2_pinB <<= 1;

		if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_8) == 0) rotary_1_pinA++;
		if (LL_GPIO_IsInputPinSet(GPIOA, LL_GPIO_PIN_9) == 0) rotary_1_pinB++;
		if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_6) == 0) rotary_2_pinA++;
		if (LL_GPIO_IsInputPinSet(GPIOB, LL_GPIO_PIN_7) == 0) rotary_2_pinB++;

		if (rotary_1_pinA == 0x80)
		{
			if (rotary_1_pinB == 0x00) rotary_1_updown = -1;
			if (rotary_1_pinB == 0xFF) rotary_1_updown = 1;
		}

		if (rotary_2_pinA == 0x80)
		{
			if (rotary_2_pinB == 0x00) rotary_2_updown = -1;
			if (rotary_2_pinB == 0xFF) rotary_2_updown = 1;
		}


		if (function_button_encoder1 == TIMEBASE_MODE)							//TIMEBASE_MODE
		{
			timebase += rotary_1_updown;
			if (timebase < 0) timebase = 0;
			if (timebase > 12) timebase = 12;

			switch(timebase)													//set TIM3 to required timebase setting
			{
			case 0:
				LL_TIM_SetPrescaler(TIM3, 99);
				LL_TIM_SetAutoReload(TIM3, 7499);								// 200ms/div
				break;
			case 1:
				LL_TIM_SetPrescaler(TIM3, 99);
				LL_TIM_SetAutoReload(TIM3, 3749);								// 100ms/div
				break;
			case 2:
				LL_TIM_SetPrescaler(TIM3, 99);
				LL_TIM_SetAutoReload(TIM3, 1874);								// 50ms/div
				break;
			case 3:
				LL_TIM_SetPrescaler(TIM3, 99);
				LL_TIM_SetAutoReload(TIM3, 749);								// 20ms/div
				break;
			case 4:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 37499);								// 10ms/div
				break;
			case 5:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 18749);								// 5ms/div
				break;
			case 6:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 7499);								// 2ms/div
				break;
			case 7:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 3749);								// 1ms/div
				break;
			case 8:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 1874);								// 500us/div
				break;
			case 9:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 749);								// 200us/div
				break;
			case 10:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 374);								// 100us/div
				break;
			case 11:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 187);								// 50us/div
				break;
			case 12:
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 74);									// 20us/div
				break;
			default:															//should never happen
				LL_TIM_SetPrescaler(TIM3, 0);
				LL_TIM_SetAutoReload(TIM3, 1999);
				break;
			}
		}
		else if (function_button_encoder1 == TRIGGER_LEVEL_MODE)				//TRIGGER_LEVEL_MODE																//function_button_encoder1 == TRIGGER_LEVEL_MODE
		{
			trigger_level -= (4 * rotary_1_updown);								//trigger_level 4 .. 124 in steps of 4
			if (trigger_level < 4) trigger_level = 4;							//from top to bottom of screen
			if (trigger_level > 124) trigger_level = 124;
		}

		else																	//TRIGGER_CHANNEL_MODE
		{
			trigger_channel += rotary_1_updown;
			if (trigger_channel > 1) trigger_channel = 1;
			if (trigger_channel < 0) trigger_channel = 0;
		}




		if (function_button_encoder2 == ATTENUATOR1_MODE)						//ATTENUATOR1_MODE
		{
			attenuator_ch1 += rotary_2_updown;									//attenuator levels 0, 1, 2
			if (attenuator_ch1 < 0) attenuator_ch1 = 0;							//500mV, 1V, 2V/division
			if (attenuator_ch1 > 2) attenuator_ch1 = 2;

			switch(attenuator_ch1)
			{
			case 0:
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);						//switch A off, B off
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);						//one 82K resistor on opamp
				break;
			case 1:
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);						//switch A on, B off
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);					//two 82k parallel
				break;
			case 2:
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_4);					//switch A on, B on
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_5);					//two 82k parallel and 39k parallel
				break;

			default:
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4);
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_5);
				break;
			}
		}

		else if (function_button_encoder2 == ATTENUATOR2_MODE)					//ATTENUATOR2_MODE
		{
			attenuator_ch2 += rotary_2_updown;
			if (attenuator_ch2 < 0) attenuator_ch2 = 0;
			if (attenuator_ch2 > 2) attenuator_ch2 = 2;

			switch(attenuator_ch2)
			{
			case 0:
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_6);
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_7);
				break;
			case 1:
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_6);
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_7);
				break;
			case 2:
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_6);
				LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_7);
				break;

			default:
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_6);
				LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_7);
				break;
			}
		}
		else 																	//VIRTUAL_GROUND_MODE
		{
			virtual_ground += rotary_2_updown;
			if (virtual_ground < 1) virtual_ground = 1;
			if (virtual_ground > 126) virtual_ground = 126;

			LL_TIM_OC_SetCompareCH3(TIM4, (virtual_ground * 10));
		}
	}
}



void EXTI9_5_IRQHandler(void)
{
	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_6) != RESET)						//channel settings
	{
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_6);
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_6)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_6);

		LL_TIM_EnableIT_UPDATE(TIM2);


		timeout = 300;
		LL_SYSTICK_EnableIT();
	}

	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_8) != RESET)						//timebase settings
	{
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_8);
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_8)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_8);

		LL_TIM_EnableIT_UPDATE(TIM2);


		timeout = 300;
		LL_SYSTICK_EnableIT();
	}
}


//buttons handling
void EXTI15_10_IRQHandler(void)
{
	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_11) != RESET)					//TIMEBASE/TRIGGER_LEVEL button active
	{
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_11);
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_11)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_11);

		if (function_button_encoder1 == TIMEBASE_MODE)							//TIMEBASE_MODE
		{
			function_button_encoder1 = TRIGGER_LEVEL_MODE;						//go to ATTENUATOR1_MODE
			timebase_color = ST7735_WHITE;
			trigger_level_color = ST7735_YELLOW;
			trigger_channel_color = ST7735_WHITE;
		}
		else if (function_button_encoder1 == TRIGGER_LEVEL_MODE)				//TRIGGER_LEVEL_MODE
		{
			function_button_encoder1 = TRIGGER_CHANNEL_MODE;					// go to TRIGGER_CHANNEL_MODE
			timebase_color = ST7735_WHITE;
			trigger_level_color = ST7735_WHITE;
			trigger_channel_color = ST7735_YELLOW;
		}
		else 																	//TRIGGER_CHANNEL_MODE
		{
			function_button_encoder1 = TIMEBASE_MODE;							//go to TIMEBASE_MODE
			timebase_color = ST7735_YELLOW;
			trigger_level_color = ST7735_WHITE;
			trigger_channel_color = ST7735_WHITE;
		}

		timeout = 300;
		LL_SYSTICK_EnableIT();
	}

	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_12) != RESET)					//CHANNEL_FUNCTION button active
	{
		LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_12);
		while (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_12)) LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_12);

		if (function_button_encoder2 == ATTENUATOR1_MODE)						//ATTENUATOR1_MODE
		{
			function_button_encoder2 = ATTENUATOR2_MODE;						//go to ATTENUATOR2_MODE
			attenuator_ch1_color = ST7735_WHITE;
			attenuator_ch2_color = ST7735_YELLOW;
			virtual_ground_color = ST7735_WHITE;
		}
		else if (function_button_encoder2 == ATTENUATOR2_MODE)					//ATTENUATOR2_MODE
		{
			function_button_encoder2 = VIRTUAL_GROUND_MODE;						//go to VIRTUAL_GROUND_MODE
			attenuator_ch1_color = ST7735_WHITE;
			attenuator_ch2_color = ST7735_WHITE;
			virtual_ground_color = ST7735_YELLOW;
		}
		else 																	//VIRTUAL_GROUND_MODE
		{
			function_button_encoder2 = ATTENUATOR1_MODE;						// go to ATTENUATOR1_MODE
			attenuator_ch1_color = ST7735_YELLOW;
			attenuator_ch2_color = ST7735_WHITE;
			virtual_ground_color = ST7735_WHITE;
		}
		timeout = 300;
		LL_SYSTICK_EnableIT();													//enable timeout timer (systick)
	}
}
