/*
 * dual trace oscilloscope with a blue pill (STM32F103C8)
 * MUCH overclocked on 120 MHz with the ADC on 30 MHz
 * 1.8 inch TFT screen 160x128 pixel resolution
 *
 * rotary Encoder for setting of CH1/CH2 ATTENUATION and GND_level
 * rotary Encoder for setting of TIMEBASE and TRIGGERLEVEL
 *
 * timebase from 20us to 200ms / division
 * sensitivity from 500mV to 2V / division
 *
 * XTAL now 8MHz
 */


#define BUFFERSIZE 320
#define LASTESTTRIGPOINT 160
#define X_SIZE 160
#define Y_SIZE 128
#define X_MAX 159
#define Y_MAX 127

#define TIMEBASE_MODE 			0
#define TRIGGER_LEVEL_MODE 		1
#define TRIGGER_CHANNEL_MODE	2

#define ATTENUATOR1_MODE 		0
#define ATTENUATOR2_MODE 		1
#define OFFSET_MODE 			2


#include "main.h"



void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_ADC2_Init(void);
static void MX_SPI2_Init(void);

static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);

void DrawRaster(void);


uint32_t adc_buffer[BUFFERSIZE];
uint16_t trace_1_buffer[BUFFERSIZE];
uint16_t trace_2_buffer[BUFFERSIZE];
uint16_t erase_1_buffer[BUFFERSIZE];
uint16_t erase_2_buffer[BUFFERSIZE];

volatile int8_t conversion_ready;												//0, 1
volatile int8_t function_button_encoder1;										//0, 1			timebase, triggerlevel
volatile int8_t function_button_encoder2;										//0, 1, 2, 3  	atten1, atten2, virtual_ground
volatile int8_t timebase;														//0 .. 13		20us .. 200ms / division
volatile int16_t trigger_level;													//0 .. 127		top to bottom of screen
volatile int8_t trigger_channel;												//0, 1			channel1, channel2

volatile int8_t attenuator_ch1;													//0, 1, 2, 3	500mV, 1V, 2V, 5V / division
volatile int8_t attenuator_ch2;													//0, 1, 2, 3	500mV, 1V, 2V, 5V / division
volatile int16_t virtual_ground;												//0 .. 127		top to bottom of screen

volatile uint16_t attenuator_ch1_color;
volatile uint16_t attenuator_ch2_color;
volatile uint16_t timebase_color;
volatile uint16_t trigger_level_color;
volatile uint16_t trigger_channel_color;
volatile uint16_t virtual_ground_color;


int main(void)
{
	uint16_t temp1, temp2;

	uint16_t i, j;

	uint16_t x1, y1, z1;
	uint16_t x2, y2, z2;
	uint16_t x3, y3, z3;
	uint16_t x4, y4, z4;

	uint16_t trigger_point;
	uint16_t previous_trigger_point;
	uint16_t previous_trigger_level;

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	LL_GPIO_AF_Remap_SWJ_NOJTAG();

	SystemClock_Config();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC1_Init();
	MX_ADC2_Init();
	MX_SPI2_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_TIM4_Init();


	LL_ADC_Enable(ADC1);
	LL_ADC_StartCalibration(ADC1);
	while (LL_ADC_IsCalibrationOnGoing(ADC1));
	LL_mDelay(10);

	LL_ADC_Enable(ADC2);
	LL_ADC_StartCalibration(ADC2);
	while (LL_ADC_IsCalibrationOnGoing(ADC2));
	LL_mDelay(10);

	LL_TIM_EnableCounter(TIM2);													//10 kHz irqs for debouncing

	LL_TIM_CC_EnableChannel(TIM4, LL_TIM_CHANNEL_CH3);
	LL_TIM_CC_EnableChannel(TIM4, LL_TIM_CHANNEL_CH4);
	LL_TIM_EnableCounter(TIM4);													//virtual_ground OPAMPS


	LL_SPI_Enable(SPI2);														//control DISPLAY

	ST7735_Init();
	ST7735_FillScreen(ST7735_BLACK);

	//default settings
	timebase = 9;																//timebase on 200us/div
	LL_TIM_SetPrescaler(TIM3, 0);
	LL_TIM_SetAutoReload(TIM3, 749);

	conversion_ready = 0;														//wait for conversion to complete
	function_button_encoder1 = TIMEBASE_MODE;
	function_button_encoder2 = ATTENUATOR1_MODE;

	attenuator_ch1 = 0;															//2V/div
	attenuator_ch2 = 0;

	virtual_ground = 64;														//0V

	trigger_level = 64;															//middle of screen
	previous_trigger_level = 64;

	trigger_point = 1;															//start of array
	previous_trigger_point = 1;

	attenuator_ch1_color = ST7735_YELLOW;
	attenuator_ch2_color = ST7735_WHITE;
	timebase_color = ST7735_YELLOW;
	trigger_level_color = ST7735_WHITE;
	trigger_channel_color = ST7735_WHITE;
	virtual_ground_color = ST7735_WHITE;




	LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t) &ADC1->DR);
	LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_1, (uint32_t) &adc_buffer);
	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, BUFFERSIZE);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);									//Enable IRQ at transfer complete
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);

	LL_ADC_REG_StartConversionExtTrig(ADC1, LL_ADC_REG_TRIG_EXT_RISING);
	LL_ADC_REG_StartConversionExtTrig(ADC2, LL_ADC_REG_TRIG_EXT_RISING);
	LL_TIM_EnableCounter(TIM3);													//triggering ADCs for selected timebase setting

	while (1)
	{
		//data processing
		if (conversion_ready == 1)												//ADC's are ready
		{
			for (i = 0; i < BUFFERSIZE; i++)									//copy data from adc_buffer to display_buffer
			{																	//the adc is continuously converting
				trace_1_buffer[i] = (uint16_t) (adc_buffer[i] >> 21);			//separate both channels
				trace_2_buffer[i] = (uint16_t) ((adc_buffer[i] & 0x0000FFFF) >> 5);

				//corrections because of the 3.3V reference of the ADCs
				temp1 = trace_1_buffer[i];
				trace_1_buffer[i] = (temp1 + (temp1 >> 2) - 16);
				if (trace_1_buffer[i] > 9999) trace_1_buffer[i] = 0;			// >9999 means negative
				if (trace_1_buffer[i] > 127) trace_1_buffer[i] = 127;

				temp2 = trace_2_buffer[i];
				trace_2_buffer[i] = (temp2 + (temp2 >> 2) - 16);
				if (trace_2_buffer[i] > 9999) trace_2_buffer[i] = 0;
				if (trace_2_buffer[i] > 127) trace_2_buffer[i] = 127;


			}

			i = 1;																//find trigger_point
			if (trigger_channel == 0)											//with channel 1
			{
				while ((trace_1_buffer[i - 1] < trigger_level) || (trace_1_buffer[i] >= trigger_level)) i++;
			}
			else																//with channel 2
			{
				while ((trace_2_buffer[i - 1] < trigger_level) || (trace_2_buffer[i] >= trigger_level)) i++;
			}
			if (i > X_MAX) trigger_point = 1;									//if the triggerpoint isn't found
			else trigger_point = i;												//show data from beginning of arrays


			DrawRaster();														//draw the raster before drawing the traces

			ST7735_HLine(1, 10, previous_trigger_level, ST7735_BLACK);			//draw small trigger-level line
			ST7735_HLine(1, 10, (trigger_level), trigger_level_color);
			previous_trigger_level = trigger_level;


			for (i = 1; i < X_MAX; i++)
			{
				j = i + 1;

				x1 = i + previous_trigger_point;
				x2 = x1 + 1;
				x3 = i + trigger_point;
				x4 = x3 + 1;

				y1 = erase_1_buffer[x1];
				y2 = erase_1_buffer[x2];

				y3 = trace_1_buffer[x3];
				y4 = trace_1_buffer[x4];

				z1 = erase_2_buffer[x1];
				z2 = erase_2_buffer[x2];

				z3 = trace_2_buffer[x3];
				z4 = trace_2_buffer[x4];

				//normal y-t operation
				//ST7735_Line(i, y1, j, y2, ST7735_BLACK);						//remove old trace
				//ST7735_Line(i, z1, j, z2, ST7735_BLACK);						//remove old trace

				//ST7735_Line(i, y3, j, y4, ST7735_GREEN);						//show new trace
				//ST7735_Line(i, z3, j, z4, ST7735_MAGENTA);					//show new trace


				//alternate x-y operation
				ST7735_Line(y1, z1, y2, z2, ST7735_BLACK);						//remove old trace
				ST7735_Line(y3, z3, y4, z4, ST7735_GREEN);						//show new trace

			}


			for (i = 0; i < BUFFERSIZE; i++)									//store values, used next time to erase the old trace
			{
				erase_1_buffer[i] = trace_1_buffer[i];
				erase_2_buffer[i] = trace_2_buffer[i];
			}
			previous_trigger_point = trigger_point;

			conversion_ready = 0;												//start next sequence of conversion
			LL_DMA_ClearFlag_TE1(DMA1);											//just in case...
			LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_1);						//disable DMA so that it can be
			LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, BUFFERSIZE);			//set to the number of transfers
			LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);						//re-enable DMA

			LL_TIM_EnableCounter(TIM3);											//and THEN start TIM3 --> triggers the ADCs
		}


		//draw text after raster and traces
		//upper text line
		if (attenuator_ch1 == 0)      ST7735_WriteString(1, 106, "CH1 2V   ", Font_7x10, attenuator_ch1_color, ST7735_BLACK);
		else if (attenuator_ch1 == 1) ST7735_WriteString(1, 106, "CH1 1V   ", Font_7x10, attenuator_ch1_color, ST7735_BLACK);
		else                          ST7735_WriteString(1, 106, "CH1 500mV", Font_7x10, attenuator_ch1_color, ST7735_BLACK);

		if (timebase == 0)       ST7735_WriteString(75, 106, "200ms", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 1)  ST7735_WriteString(75, 106, "100ms", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 2)  ST7735_WriteString(75, 106, "50ms ", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 3)  ST7735_WriteString(75, 106, "20ms ", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 4)  ST7735_WriteString(75, 106, "10ms ", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 5)  ST7735_WriteString(75, 106, "5ms  ", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 6)  ST7735_WriteString(75, 106, "2ms  ", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 7)  ST7735_WriteString(75, 106, "1ms  ", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 8)  ST7735_WriteString(75, 106, "500us", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 9)  ST7735_WriteString(75, 106, "200us", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 10) ST7735_WriteString(75, 106, "100us", Font_7x10, timebase_color, ST7735_BLACK);
		else if (timebase == 11) ST7735_WriteString(75, 106, "50us ", Font_7x10, timebase_color, ST7735_BLACK);
		else                     ST7735_WriteString(75, 106, "20us ", Font_7x10, timebase_color, ST7735_BLACK);

		if (trigger_channel == 0) ST7735_WriteString(120, 106, "T_CH1", Font_7x10, trigger_channel_color, ST7735_BLACK);
		else                      ST7735_WriteString(120, 106, "T_CH2", Font_7x10, trigger_channel_color, ST7735_BLACK);


		//lower text line
		if (attenuator_ch2 == 0)      ST7735_WriteString(1, 117, "CH2 2V   ", Font_7x10, attenuator_ch2_color, ST7735_BLACK);
		else if (attenuator_ch2 == 1) ST7735_WriteString(1, 117, "CH2 1V   ", Font_7x10, attenuator_ch2_color, ST7735_BLACK);
		else                          ST7735_WriteString(1, 117, "CH2 500mV", Font_7x10, attenuator_ch2_color, ST7735_BLACK);

		ST7735_WriteString(75, 117, "GND", Font_7x10, virtual_ground_color, ST7735_BLACK);

		ST7735_WriteString(120, 117, "T_LVL", Font_7x10, trigger_level_color, ST7735_BLACK);
	}
}


void DrawRaster(void)
{
	//show raster
	ST7735_HLine(0, 159, 0, ST7735_GRAY);
	ST7735_HLine(0, 159, 21, ST7735_GRAY);
	ST7735_HLine(0, 159, 42, ST7735_GRAY);
	ST7735_HLine(0, 159, 63, ST7735_GRAY);
	ST7735_HLine(0, 159, 84, ST7735_GRAY);
	ST7735_HLine(0, 159, 105, ST7735_GRAY);
	ST7735_HLine(0, 159, 127, ST7735_GRAY);

	ST7735_VLine(0, 0, 127, ST7735_GRAY);
	ST7735_VLine(31, 0, 127, ST7735_GRAY);
	ST7735_VLine(63, 0, 127, ST7735_GRAY);
	ST7735_VLine(95, 0, 127, ST7735_GRAY);
	ST7735_VLine(127, 0, 127, ST7735_GRAY);
	ST7735_VLine(159, 0, 127, ST7735_GRAY);
}




void SystemClock_Config(void)													// XTAL 8 or 12 MHz
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
	if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2) Error_Handler();
	LL_FLASH_EnablePrefetch();
	LL_RCC_HSE_Enable();
	while(LL_RCC_HSE_IsReady() != 1);

	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_15);// 120 MHz with 8 MHz xtal
	//LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_10);	// 120 MHz with 12 MHz xtal

	LL_RCC_PLL_Enable();
	while(LL_RCC_PLL_IsReady() != 1);
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);
	LL_Init1msTick(120000000);
	LL_SetSystemCoreClock(120000000);											//MCU is very much overclocked (120 MHz)
	LL_RCC_SetADCClockSource(LL_RCC_ADC_CLKSRC_PCLK2_DIV_4);					//the ADC's are overclocked too (30 MHz)
}


static void MX_ADC1_Init(void)
{
	LL_ADC_InitTypeDef ADC_InitStruct = {0};
	LL_ADC_CommonInitTypeDef ADC_CommonInitStruct = {0};
	LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
	LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PRIORITY_HIGH);
	LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_NORMAL);
	LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PERIPH_NOINCREMENT);
	LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MEMORY_INCREMENT);
	LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PDATAALIGN_WORD);
	LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MDATAALIGN_WORD);


	ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
	ADC_InitStruct.SequencersScanMode = LL_ADC_SEQ_SCAN_DISABLE;
	LL_ADC_Init(ADC1, &ADC_InitStruct);

	ADC_CommonInitStruct.Multimode = LL_ADC_MULTI_DUAL_REG_SIMULT;
	LL_ADC_CommonInit(__LL_ADC_COMMON_INSTANCE(ADC1), &ADC_CommonInitStruct);

	ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_EXT_TIM3_TRGO;
	ADC_REG_InitStruct.SequencerLength = LL_ADC_REG_SEQ_SCAN_DISABLE;
	ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
	ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
	ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_UNLIMITED;
	LL_ADC_REG_Init(ADC1, &ADC_REG_InitStruct);

	LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_0);
	LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_0, LL_ADC_SAMPLINGTIME_1CYCLE_5);
}


static void MX_ADC2_Init(void)
{
	LL_ADC_InitTypeDef ADC_InitStruct = {0};
	LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC2);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
	ADC_InitStruct.SequencersScanMode = LL_ADC_SEQ_SCAN_DISABLE;
	LL_ADC_Init(ADC2, &ADC_InitStruct);

	ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_SOFTWARE;
	ADC_REG_InitStruct.SequencerLength = LL_ADC_REG_SEQ_SCAN_DISABLE;
	ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
	ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
	ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_NONE;
	LL_ADC_REG_Init(ADC2, &ADC_REG_InitStruct);

	LL_ADC_REG_SetSequencerRanks(ADC2, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_1);
	LL_ADC_SetChannelSamplingTime(ADC2, LL_ADC_CHANNEL_1, LL_ADC_SAMPLINGTIME_1CYCLE_5);
}


static void MX_SPI2_Init(void)
{
	LL_SPI_InitTypeDef SPI_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_13|LL_GPIO_PIN_15;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	SPI_InitStruct.TransferDirection = LL_SPI_FULL_DUPLEX;
	SPI_InitStruct.Mode = LL_SPI_MODE_MASTER;
	SPI_InitStruct.DataWidth = LL_SPI_DATAWIDTH_8BIT;
	SPI_InitStruct.ClockPolarity = LL_SPI_POLARITY_LOW;
	SPI_InitStruct.ClockPhase = LL_SPI_PHASE_1EDGE;
	SPI_InitStruct.NSS = LL_SPI_NSS_SOFT;
	SPI_InitStruct.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV8;
	SPI_InitStruct.BitOrder = LL_SPI_MSB_FIRST;
	SPI_InitStruct.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
	SPI_InitStruct.CRCPoly = 10;
	LL_SPI_Init(SPI2, &SPI_InitStruct);
}



static void MX_TIM2_Init(void)													//debouncing
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);

	NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM2_IRQn);

	TIM_InitStruct.Prescaler = 119;												//1 MHz
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 99;												//10 kHz
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM2, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM2);
	LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM2);
}


static void MX_TIM3_Init(void)													//triggering ADC
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);

	NVIC_SetPriority(TIM3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM3_IRQn);

	TIM_InitStruct.Prescaler = 0;												// 120 MHz
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 749;											// default setting 200us/div
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM3, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM3);
	LL_TIM_SetClockSource(TIM3, LL_TIM_CLOCKSOURCE_INTERNAL);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_UPDATE);
	LL_TIM_DisableMasterSlaveMode(TIM3);
}


static void MX_TIM4_Init(void)													//virtual_ground OPAMPS (PWM)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM4);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_8;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 1280;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
	LL_TIM_Init(TIM4, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM4);
	LL_TIM_SetClockSource(TIM4, LL_TIM_CLOCKSOURCE_INTERNAL);

	LL_TIM_OC_EnablePreload(TIM4, LL_TIM_CHANNEL_CH3);
	TIM_OC_InitStruct.OCMode = LL_TIM_OCMODE_PWM2;
	TIM_OC_InitStruct.OCState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.OCNState = LL_TIM_OCSTATE_DISABLE;
	TIM_OC_InitStruct.CompareValue = 640;
	TIM_OC_InitStruct.OCPolarity = LL_TIM_OCPOLARITY_HIGH;
	LL_TIM_OC_Init(TIM4, LL_TIM_CHANNEL_CH3, &TIM_OC_InitStruct);
	LL_TIM_OC_DisableFast(TIM4, LL_TIM_CHANNEL_CH3);

	LL_TIM_SetTriggerOutput(TIM4, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM4);
}


static void MX_DMA_Init(void)
{
	LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

	NVIC_SetPriority(DMA1_Channel1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}


static void MX_GPIO_Init(void)
{
	LL_EXTI_InitTypeDef EXTI_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_4 | LL_GPIO_PIN_5 | LL_GPIO_PIN_6 | LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;									//to attenuators (CD4066)
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_13;										//LED for debugging
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_10|LL_GPIO_PIN_11|LL_GPIO_PIN_12;			//SPI to display
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_MEDIUM;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);



	GPIO_InitStruct.Pin = LL_GPIO_PIN_7;										//rotary encoder1 pin B
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_9;										//rotary encoder2 pin B
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTB, LL_GPIO_AF_EXTI_LINE6);		//EXTI lines to rotary encoders
	LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTA, LL_GPIO_AF_EXTI_LINE8);
	LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTA, LL_GPIO_AF_EXTI_LINE11);
	LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTA, LL_GPIO_AF_EXTI_LINE12);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_6;									//rotary encoder1 pin A
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_8;									//rotary encoder2 pin A
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_11;								//rotary encoder1 button
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_12;								//rotary encoder2 button
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	LL_GPIO_SetPinPull(GPIOB, LL_GPIO_PIN_6, LL_GPIO_PULL_UP);
	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_8, LL_GPIO_PULL_UP);
	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_11, LL_GPIO_PULL_UP);
	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_12, LL_GPIO_PULL_UP);

	LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_6, LL_GPIO_MODE_INPUT);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_8, LL_GPIO_MODE_INPUT);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_11, LL_GPIO_MODE_INPUT);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_12, LL_GPIO_MODE_INPUT);


	NVIC_SetPriority(EXTI9_5_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(EXTI9_5_IRQn);
	NVIC_SetPriority(EXTI15_10_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(EXTI15_10_IRQn);

	LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_4 | LL_GPIO_PIN_5 | LL_GPIO_PIN_6 | LL_GPIO_PIN_7);
	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_1);
	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_10 | LL_GPIO_PIN_11 | LL_GPIO_PIN_12);
}

void Error_Handler(void)
{

}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif
